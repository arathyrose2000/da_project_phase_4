EXAMPLE TUPLES SHOWING THE CONSISTENT STATE OF THE FINAL RELATIONAL MODEL

1. SOLDIER PERSONAL DETAILS
Soldier
ID
Soldier
First name
Soldier Last
name
Soldier
Middle
initial
Sex Aadhar
number
Marital
Status
Highest Educational
Qualification
1001 Tom Brown K M 3872 3826
5932
Married Graduate
1002 Kate Silvester M F 3876 3321
5934
Single Graduate
1003 Pahul Singh C M 9876 3251
5945
Single Graduate
1004 Jimmy John M 5477 3951
5911
Single Post-graduate
1005 Sai Reddy P M 5378 2951
5993
Single Graduate
1006 Pinkie Grower F 2826 3377
3764
Single Graduate
1007 Kiran Jain M 7337 4970
2838
Married Doctorate
1008 Kunwar Jain M 9737 2923
2838
Single Graduate
1009 Ali Mark M 9127 2737
2812
Single Graduate
2. SOLDIER ADDRESS DETAILS
Soldier
ID
Building
no
Street
no
Locality City Zip
Code
District State
1001 12 12 Gachibowli Hyderabad 500032 Hyderabad Telangana
1002 32 64 Ghatkopar Mumbai 400086 Mumbai Maharashtra
1003 25 42 Pitam Pura New Delhi 110034 New Delhi New Delhi
1004 74 31 Borivali West Mumbai 400092 Mumbai Maharashtra
1005 19 76 Gota Ahmedabad 382481 Ahmedabad Gujarat
1006 10 32 Bargarpet Kolar 560000 Bangaluru Karnataka
1007 42 53 Behala Kolkata 700034 Kolkata West Bengal
1008 42 53 Behala Kolkata 700034 Kolkata West Bengal
1009 12 31 Gota Ahmedabad 382481 Ahmedabad Gujarat 
3. SOLDIER PHONE NUMBER DETAILS
Soldier ID Phone Numbers
1001 +91-9722615151
1001 +91-9724122222
1002 +91-8736211318
1003 +91-9744859215
1003 +91-9744859215
1004 +91-9637277713
1004 +91-9753177223
1005 +91-6286635334
1006 +91-9696969696
1007 +91-6836266966
1008 +91-6839796962
1009 +91-6832373775
4. SOLDIER EMAIL ID DETAILS
Soldier ID E-mail ID
1001 tbrown@gmail.com
1002 kate.m@hotmail.com
1002 katey@gmail.com
1003 phul99@gmail.com
1003 pahul.singh.c@outlook.com
1004 jimmy2019@gmail.com
1004 john_jim@gmail.com
1005 preddy1996@outlook.com
1006 pinkie1992@gmail.com
1007 kjain1927@outlook.com
1007 kjain1927@gmail.com
1008 kunwar.jain97@gmail.com
1009 alimark1999@outlook.com
5. SOLDIER PARENTAL DETAILS
Soldier
ID
Father’s First
name
Father’s Last
name
Father’s Middle
initial
Mother’s First
name
Mother’s Last
name
Mother’s Middle
initial
1001 Jamie Brown K Navya Brown H
1002 Kim Silvester Rinny Silvester S
1003 Pal Singh Sanya Singh
1004 Jim John Eva John Z 
1005 Harsha Reddy S Tejaswi Reddy
1006 Rohan Grower Sara Grower V
1007 Kabir Jain Anika Jain
1008 Kiran Jain Seema Jain L
1009 Mohammed Mark Fathima Mark
6. SOLDIER DATES
Soldier ID Date of birth Date of recruitment Date of death
1001 14-02-1979 18-10-1996 NULL
1002 15-09-1975 15-12-1996 17-08-2019
1003 28-02-1976 29-02-1997 NULL
1004 12-10-1960 14-12-1980 NULL
1005 13-02-1976 15-02-1998 NULL
1006 08-09-1972 01-01-1994 NULL
1007 02-07-1973 02-10-1995 NULL
1008 02-07-1977 02-10-1997 NULL
1009 24-04-1978 18-09-1995 NULL
7. SOLDIER MILITARY ACCOMPLISHMENTS
Soldier ID Military accomplishments
1001 Killed multiple terrorists at XXX
1001 Worked as Division Commander 2019-
1002 Defended the Northeastern Indian border
1004 Worked as Division Commander 2000-2006
1004 Worked as Brigade Commander 2006-2019
1005 Worked as Division Commander 2000-2006
1005 Worked as Brigade Commander 2006-2010
1005 Works as General Officer Commanding-in-Chief 2010-
1006 Worked as Division Commander 2008-2017
1006 Works as Brigade commander 2017-
1007 Worked as Division Commander 2000-2006
1007 Worked as Brigade Commander 2006-2007
1007 Worked as General Officer Commanding-in-Chief 2007-2017
1007 Works as Chief of Army Staff 2017-
1008 Works as Division Commander 2018- 
8. SOLDIER MEDICAL DETAILS
Soldier ID Height (in cm) Weight (in kg) Blood group Date of medical test
1001 170 75 O+ 14-10-2019
1002 155 50 A+ 14-10-2019
1003 173 70 AB+ 14-10-2019
1004 168 55 O- 14-10-2019
1005 165 57 A- 14-10-2019
1006 160 56 B+ 14-10-2019
1007 162 69 B- 14-10-2019
1008 168 55 B- 14-10-2019
1009 175 70 A+ 14-10-2019
9. SOLDIER MILITARY DETAILS
Soldier ID Soldier Rank ID Division ID Brigade/Corp ID Command ID Date of retirement
1001 8 1 1 1 NULL
1002 NULL NULL NULL NULL NULL
1003 13 3 1 1 NULL
1004 NULL NULL NULL NULL 12-10-2019
1005 3 1 1 1 NULL
1006 5 1 1 1 NULL
1007 2 1 1 1 NULL
1008 7 3 1 1 NULL
1009 15 3 1 1 NULL
10. SALARY
Soldier Rank ID Division ID Brigade/Corp ID Command ID Salary
8 1 1 1 238153
13 3 1 1 135148
3 1 1 1 388102
5 1 1 1 308717
2 1 1 1 388102
7 3 1 1 282555
15 3 1 1 113022
11. PENSION
Date of retirement Pension
NULL NULL
12-10-2019 1000000
12. DEPENDENTS
Dependent First
Name
Dependent Last
Name
Dependent
Middle Initial
Sponsor
Soldier ID
Se
x
Relationship
with the
sponsor
Date of Birth
Tanya Brown B 1001 F Spouse 15-02-1983
Kimley Silvester M 1002 M Brother 20-03-1944
Jamie Brown K 1001 M Father 10-10-1955
Navya Brown H 1001 F Mother 09-11-1956
Anika Jain 1007 F Mother 08-10-1957
Seema Jain L 1008 F Mother 07-10-1946
Sandra Jain 1008 F Sister 25-05-1979
Mohammed Mark 1009 M Father 09-09-1949
Fathima Mark 1009 F Mother 08-08-1950
Sanjana Mark 1009 F Sister 23-09-1978
Isha Mark 1009 F Sister 23-10-1977
Ayesha Mark 1009 F Sister 12-01-1976
Harsha Reddy S 1005 M Father 10-12-1952
Tejaswi Reddy 1005 F Mother 01-11-1955
Rohan Grower 1006 M father 08-09-1952
Sara Grower V 1006 F Mother 07-03-1950
Purul Grower 1006 M Brother 08-10-1980
Alan Jain 1007 M Son 08-10-2005
13. SOLDIER RANKS
Soldier
Rank ID
Soldier Rank Name Rank Insignia Base
salary
1 Field Marshal National emblem over a crossed baton and saber in a lotus
blossom wreath
250000
2 General National emblem over a five-pointed star, both over a crossed
baton and saber
220000
3 Lieutenant General National emblem over crossed baton and saber 200000
4 Major General Five-pointed star over crossed baton and saber 180000
5 Brigadier National emblem over three five-pointed stars in a triangular
formation
175000
6 Colonel National emblem over two five-pointed stars 169000
7 Lieutenant Colonel National emblem over five-pointed star 150000
8 Major National emblem 135000
9 Captain Three five-pointed stars 100000
10 Lieutenant Two five-pointed stars 95000
11 Missioned Officers of the
Indian Army
Gold national emblem with stripe 90000
12 Subedar (Infantry) or
Risaldar (Cavalry and
Armoured Regiments)
Two gold stars with stripe 85000
13 Naib Subedar (Infantry) or
Naib Risaldar (Cavalry and
Armoured Regiments)
One gold star with stripe 80000
14 Havildar (Infantry) or
Daffadar (Cavalry and
Armoured Regiments)
Three rank chevrons 70000
15 Naik (Infantry) or Lance
Daffadar (Cavalry and
Armoured Regiments)
Two rank chevrons 60000
16 Lance Naik (Infantry) or
Acting Lance Daffadar
(Cavalry and Armoured
Regiments)
One rank chevron 50000
17 Sepoy Plain shoulder badge 40000
14. COMMAND
Command ID Command name General Officer Commanding-in-Chief Soldier ID Salary increment
1 General Command 1005 30
2 Eastern Command NIL 20
3 Northern Command NIL 35
4 Southern Command NIL 25
5 South-Western Command NIL 30
6 Western NIL 19
7 Army Training Command NIL 40
15. BRIGADE/CORP
Brigade/Corp ID Brigade/Corp Name Command ID Commander Soldier ID Salary increment
1 77th Indian Infantry Brigade 1 1006 15
16. DIVISION
Division ID Division Name Corp ID Command ID Commander Soldier ID Salary increment
1 15th Infantry Division 1 1 1001 18
2 4th Cavalry Division 1 1 NIL 26
3 4th Infantry Division 1 1 1008 13
17. CHIEF OF ARMY STAFF
Chief of Army Staff soldier ID Chronological position as the Chief of Army Staff
1007 12
18. GENERAL OFFICER COMMANDING-IN-CHIEF
Soldier ID Command ID under his control
1005 1
19. COMMANDER OF BRIGADE/CORP
Soldier ID Brigade/Corp ID under his control Command ID of the Brigade under his control
1006 1 1
20. COMMANDER OF DIVISION
Soldier
ID
Division ID under his
control
Brigade ID of the Division under his
control
Command ID of the Division under his
control
1001 1 1 1
1008 3 1 1
NOTES
 NIL values does not imply NULL. It only means that in the future when more soldiers are added, NIL
values would be filled
 The attribute values marked as derived would be calculated on the basis of certain formulas
(mentioned in the SRS) 