INSERT INTO COMMANDS
VALUES
    (1, "General Command", 1001, 30 );
INSERT INTO COMMANDS
VALUES
    (2, "Eastern Command", 1002, 20 );
INSERT INTO COMMANDS
VALUES
    (3, "Northern Command", 1003, 35 );
INSERT INTO COMMANDS
VALUES
    (4, "Southern Command", 1004, 25 );
INSERT INTO COMMANDS
VALUES
    (5, "South-Western Command", 1005, 30 );
INSERT INTO COMMANDS
VALUES
    (6, "Western", 1006, 19 );
INSERT INTO COMMANDS
VALUES
    (7, "Army Training Command", 1007, 40 );

SELECT A.BaseSalary, B.SalaryIncrement, C.SalaryIncrement, D.SalaryIncrement FROM RANKS A, COMMANDS B, CORPS C, DIVISIONS D, SALARY E, SOLDIER_MILITARY F WHERE  E.RankID=A.ID AND F.RankID=A.ID AND E.CommandID=B.CommandID AND F.CommandID=F.CommandID AND E.CorpID=C.CorpID AND F.CorpID=C.CorpID AND D.DivID=E.DivisionID AND F.DivisionID=D.DivID;